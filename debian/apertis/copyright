Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: no-info-found
License: LGPL-2.1+

Files: debian/*
Copyright: 2008-2010 Philip Withnall <philip@tecnocode.co.uk>
            2009-2010 Richard Schwarting <aquarichy@gmail.com>
            2009 Thibault Saunier <saunierthibault@gmail.com>
License: LGPL-2.1+

Files: debian/tests/installed-tests
Copyright: 2019, Simon McVittie
 2013, 2019, Canonical Ltd.
License: LGPL-2

Files: demos/*
Copyright: 2015, 2017, Philip Withnall <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: demos/docs-list/*
Copyright: 2014, Debarshi Ray <rishi.is@lostca.se>
License: LGPL-2.1+

Files: demos/docs-property/*
Copyright: 2019, Mayank Sharma <mayank8019@gmail.com>
License: LGPL-2.1+

Files: demos/scrapbook/*
Copyright: Joe Cortes 2010, <escozzia@gmail.com>
License: LGPL-2.1+

Files: gdata/*
Copyright: Philip Withnall 2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/app/*
Copyright: Philip Withnall 2010, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/app/gdata-app-categories.h
Copyright: Philip Withnall 2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/atom/*
Copyright: Philip Withnall 2009-2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/atom/gdata-author.h
 gdata/atom/gdata-category.h
 gdata/atom/gdata-generator.h
 gdata/atom/gdata-link.h
Copyright: Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/exif/*
Copyright: Richard Schwarting 2009, <aquarichy@gmail.com>
License: LGPL-2.1+

Files: gdata/gd/*
Copyright: Philip Withnall 2009-2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gd/gdata-gd-feed-link.c
Copyright: Richard Schwarting 2010, <aquarichy@gmail.com>
 Philip Withnall 2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gd/gdata-gd-feed-link.h
Copyright: Richard Schwarting 2010, <aquarichy@gmail.com>
License: LGPL-2.1+

Files: gdata/gd/gdata-gd-name.h
Copyright: Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-access-handler.c
Copyright: Philip Withnall 2009–2010, 2015 <philip@tecnocode.co.uk>
 2015, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/gdata-access-handler.h
Copyright: Philip Withnall 2009, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-access-rule.c
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
 Philip Withnall 2009–2010, 2014 <philip@tecnocode.co.uk>
 2015, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/gdata-access-rule.h
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
 Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-authorization-domain.c
 gdata/gdata-authorization-domain.h
 gdata/gdata-authorizer.h
 gdata/gdata-client-login-authorizer.c
 gdata/gdata-client-login-authorizer.h
 gdata/gdata-comment.c
 gdata/gdata-comment.h
 gdata/gdata-oauth1-authorizer.c
 gdata/gdata-oauth1-authorizer.h
Copyright: Philip Withnall 2011, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-authorizer.c
Copyright: Philip Withnall 2011, 2014 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-buffer.c
 gdata/gdata-buffer.h
 gdata/gdata-download-stream.c
 gdata/gdata-download-stream.h
 gdata/gdata-parsable.h
 gdata/gdata-parser.c
 gdata/gdata-parser.h
 gdata/gdata-upload-stream.c
 gdata/gdata-upload-stream.h
Copyright: Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-commentable.c
Copyright: Richard Schwarting 2010, <aquarichy@gmail.com>
 Philip Withnall 2008–2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-commentable.h
Copyright: Richard Schwarting 2010, <aquarichy@gmail.com>
 Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-entry.c
 gdata/gdata-entry.h
Copyright: Philip Withnall 2008–2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-feed.c
Copyright: Philip Withnall 2008–2010, <philip@tecnocode.co.uk>
 2015, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/gdata-feed.h
 gdata/gdata-private.h
 gdata/gdata-types.c
 gdata/gdata-types.h
 gdata/gdata.h
Copyright: Philip Withnall 2008-2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-goa-authorizer.c
 gdata/gdata-goa-authorizer.h
Copyright: Matthew Barnes 2011, <mbarnes@redhat.com>
License: LGPL-2.1+

Files: gdata/gdata-oauth2-authorizer.c
 gdata/gdata-oauth2-authorizer.h
Copyright: Philip Withnall 2011, 2014, 2015, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-parsable.c
 gdata/gdata-query.h
Copyright: Philip Withnall 2009–2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-query.c
Copyright: Philip Withnall 2009–2010, <philip@tecnocode.co.uk>
 2015, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/gdata-service.c
 gdata/gdata-service.h
Copyright: Philip Withnall 2008, 2009, 2010, 2014 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/gdata-version.h.in
Copyright: Holger Berndt 2011, <hb@gnome.org>
License: LGPL-2.1+

Files: gdata/georss/*
Copyright: Richard Schwarting 2009, <aquarichy@gmail.com>
License: LGPL-2.1+

Files: gdata/georss/gdata-georss-where.c
Copyright: Richard Schwarting 2009, <aquarichy@gmail.com>
 Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/media/*
Copyright: Philip Withnall 2009-2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/media/gdata-media-category.h
 gdata/media/gdata-media-credit.h
 gdata/media/gdata-media-group.h
 gdata/media/gdata-media-thumbnail.h
Copyright: Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/*
Copyright: Philip Withnall 2009-2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/calendar/*
Copyright: Philip Withnall 2009, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/calendar/gdata-calendar-access-rule.c
 gdata/services/calendar/gdata-calendar-access-rule.h
Copyright: Philip Withnall 2015, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/calendar/gdata-calendar-calendar.c
 gdata/services/calendar/gdata-calendar-event.c
 gdata/services/calendar/gdata-calendar-feed.c
Copyright: Philip Withnall 2009, 2010, 2014, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/calendar/gdata-calendar-event.h
 gdata/services/calendar/gdata-calendar-query.h
Copyright: Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/calendar/gdata-calendar-query.c
Copyright: Philip Withnall 2009, 2010, 2017, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/calendar/gdata-calendar-service.c
Copyright: Philip Withnall 2009, 2014, 2015, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/contacts/gdata-contacts-contact.c
 gdata/services/contacts/gdata-contacts-contact.h
Copyright: Philip Withnall 2009, 2010, 2011, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/contacts/gdata-contacts-group.c
 gdata/services/contacts/gdata-contacts-group.h
Copyright: Philip Withnall 2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/contacts/gdata-contacts-service.c
Copyright: Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/documents/*
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-access-rule.c
 gdata/services/documents/gdata-documents-access-rule.h
 gdata/services/documents/gdata-documents-entry-private.h
 gdata/services/documents/gdata-documents-utils.c
 gdata/services/documents/gdata-documents-utils.h
Copyright: 2015-2017, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-document.c
Copyright: Philip Withnall 2010, <philip@tecnocode.co.uk>
 2015, 2017, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-document.h
Copyright: Philip Withnall 2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-drawing.c
 gdata/services/documents/gdata-documents-pdf.c
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
 Philip Withnall 2010, <philip@tecnocode.co.uk>
 Cosimo Cecchi 2012, <cosimoc@gnome.org>
 2016, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-drawing.h
 gdata/services/documents/gdata-documents-pdf.h
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
 Philip Withnall 2010, <philip@tecnocode.co.uk>
 Cosimo Cecchi 2012, <cosimoc@gnome.org>
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-drive-query.c
 gdata/services/documents/gdata-documents-drive-query.h
 gdata/services/documents/gdata-documents-drive.c
 gdata/services/documents/gdata-documents-drive.h
Copyright: Ondrej Holy 2020, <oholy@redhat.com>
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-entry.c
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
 2015, 2016, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-feed.c
 gdata/services/documents/gdata-documents-query.c
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
 Philip Withnall 2010, <philip@tecnocode.co.uk>
 2015, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-feed.h
Copyright: Thibault Saunier <saunierthibault@gmail.com
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-folder.c
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
 Philip Withnall 2010, <philip@tecnocode.co.uk>
 2015-2017, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-metadata.c
 gdata/services/documents/gdata-documents-metadata.h
Copyright: Michael Terry 2017, <mike@mterry.name>
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-presentation.c
 gdata/services/documents/gdata-documents-spreadsheet.c
 gdata/services/documents/gdata-documents-text.c
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
 Philip Withnall 2010, <philip@tecnocode.co.uk>
 2016, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-property.c
 gdata/services/documents/gdata-documents-property.h
Copyright: Mayank Sharma <mayank8019@gmail.com>
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-service.c
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
 Philip Withnall 2010, 2014 <philip@tecnocode.co.uk>
 2015, 2016, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/services/documents/gdata-documents-upload-query.c
 gdata/services/documents/gdata-documents-upload-query.h
Copyright: Philip Withnall 2012, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/freebase/*
Copyright: 2014, Carlos Garnacho <carlosg@gnome.org>
License: LGPL-2.1+

Files: gdata/services/picasaweb/*
Copyright: Richard Schwarting 2009, <aquarichy@gmail.com>
 Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/picasaweb/gdata-picasaweb-album.c
 gdata/services/picasaweb/gdata-picasaweb-file.c
 gdata/services/picasaweb/gdata-picasaweb-query.c
 gdata/services/picasaweb/gdata-picasaweb-service.c
 gdata/services/picasaweb/gdata-picasaweb-user.c
Copyright: Richard Schwarting 2009, <aquarichy@gmail.com>
 Philip Withnall 2009-2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/picasaweb/gdata-picasaweb-comment.c
 gdata/services/picasaweb/gdata-picasaweb-comment.h
Copyright: Philip Withnall 2011, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/picasaweb/gdata-picasaweb-feed.c
Copyright: Richard Schwarting 2009-2010, <aquarichy@gmail.com>
 Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/tasks/*
Copyright: Peteris Krisjanis 2013, <pecisk@gmail.com>
License: LGPL-2.1+

Files: gdata/services/youtube/*
Copyright: Philip Withnall 2009-2010, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/youtube/gdata-youtube-category.c
 gdata/services/youtube/gdata-youtube-category.h
Copyright: Philip Withnall 2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/youtube/gdata-youtube-comment.c
Copyright: Philip Withnall 2011, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/youtube/gdata-youtube-comment.h
Copyright: Philip Withnall 2011, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/youtube/gdata-youtube-feed.c
 gdata/services/youtube/gdata-youtube-feed.h
Copyright: Philip Withnall 2015, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/youtube/gdata-youtube-query.c
Copyright: Philip Withnall 2009-2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/youtube/gdata-youtube-query.h
Copyright: Philip Withnall 2009, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/youtube/gdata-youtube-service.c
 gdata/services/youtube/gdata-youtube-video.c
Copyright: Philip Withnall 2008-2010, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/youtube/gdata-youtube-service.h
 gdata/services/youtube/gdata-youtube-video.h
Copyright: Philip Withnall 2008-2009, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/services/youtube/gdata-youtube-state.h
Copyright: Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/authorization.c
 gdata/tests/oauth1-authorizer.c
Copyright: Philip Withnall 2011, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/buffer.c
Copyright: Philip Withnall 2016, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/calendar.c
Copyright: Philip Withnall 2009, 2010, 2014, 2015, 2017, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/common.c
Copyright: Philip Withnall 2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/common.h
Copyright: Philip Withnall 2009, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/contacts.c
Copyright: Philip Withnall 2009, 2010, 2011, 2014 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/documents.c
Copyright: Thibault Saunier 2009, <saunierthibault@gmail.com>
 Philip Withnall 2010, <philip@tecnocode.co.uk>
 2015, 2016, Red Hat, Inc.
License: LGPL-2.1+

Files: gdata/tests/gdata-dummy-authorizer.c
 gdata/tests/gdata-dummy-authorizer.h
 gdata/tests/tasks.c
Copyright: Philip Withnall 2014, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/general.c
Copyright: Philip Withnall 2008-2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/oauth2-authorizer.c
Copyright: Philip Withnall 2011, 2014 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/perf.c
 gdata/tests/streams.c
Copyright: Philip Withnall 2010, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/picasaweb.c
Copyright: Richard Schwarting 2009, <aquarichy@gmail.com>
 Philip Withnall 2009-2010, <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: gdata/tests/youtube.c
Copyright: Philip Withnall 2008-2010, 2015 <philip@tecnocode.co.uk>
License: LGPL-2.1+

Files: meson.build
Copyright: no-info-found
License: LGPL-2.1

Files: docs/* docs/reference/cancellation.dia docs/reference/structure.dia gdata/tests/* gdata/tests/test.ods gdata/tests/test.odt gdata/tests/test_updated.odt
Copyright: 2008-2010 Philip Withnall <philip@tecnocode.co.uk>
 2009-2010 Richard Schwarting <aquarichy@gmail.com>
 2009 Thibault Saunier <saunierthibault@gmail.com>
License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2.1".
